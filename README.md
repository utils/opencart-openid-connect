# OpenCart OpenID Connect Module

This is an OpenID Connect module for the [OpenCart e-commerce platform](https://www.opencart.com/). It was developed by [CZ.NIC](https://nic.cz) to be used with [MojeID](https://www.mojeid.cz), but it supports any OpenID Connect identity provider.

It is intended to authenticate end customers to the front office.

## Documentation
This module can be easily installed using OpenCart's *Extension Installer* - packaged plugin releases can be downloaded [here](https://gitlab.nic.cz/utils/opencart-openid-connect/-/releases/permalink/latest). Full documentation, including installation instructions and configuration, is available [here](https://www.mojeid.cz/dokumentace/html/ImplementacePodporyMojeid/OpenidConnect/KnihovnyModuly/opencart.html).