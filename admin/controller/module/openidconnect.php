<?php

namespace Opencart\Admin\Controller\Extension\Openidconnect\Module;
//for 4.0.0.0

class Openidconnect extends \Opencart\System\Engine\Controller
{
    public function __construct(\Opencart\System\Engine\Registry $registry)
    {
        parent::__construct($registry);

        $this->config->addPath('extension/openidconnect', DIR_EXTENSION . 'openidconnect/system/config');
    }

    public function index(): void
    {
        $this->load->language('extension/openidconnect/module/openidconnect');

        $this->load->config('extension/openidconnect/openidconnect');
        $this->document->setTitle($this->language->get('heading_title'));

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $this->model_setting_setting->editSetting('openidconnect', $this->request->post);
        }

        $data = array();

        $data['error'] = '';

        $config_data = $this->model_setting_setting->getSetting('openidconnect');

        $data['openidconnect_client_id'] = array_key_exists('openidconnect_client_id', $config_data) ? $config_data['openidconnect_client_id'] : "";
        $data['openidconnect_client_secret'] = array_key_exists('openidconnect_client_secret', $config_data) ? $config_data['openidconnect_client_secret'] : "";
        $data['openidconnect_authorization_url'] = array_key_exists('openidconnect_authorization_url', $config_data) ? $config_data['openidconnect_authorization_url'] : "";
        $data['openidconnect_token_url'] = array_key_exists('openidconnect_token_url', $config_data) ? $config_data['openidconnect_token_url'] : "";
        $data['openidconnect_userinfo_url'] = array_key_exists('openidconnect_userinfo_url', $config_data) ? $config_data['openidconnect_userinfo_url'] : "";
        $data['openidconnect_scope'] = array_key_exists('openidconnect_scope', $config_data) ? $config_data['openidconnect_scope'] : "";
        $data['openidconnect_require_prompt'] = array_key_exists('openidconnect_require_prompt', $config_data) ? $config_data['openidconnect_require_prompt'] : false;

        $ssl      = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
        $sp       = strtolower($_SERVER['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port     = $_SERVER['SERVER_PORT'];
        $port     = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host     = (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
        $host     = isset($host) ? $host : $_SERVER['SERVER_NAME'] . $port;

        $data['redirect_url'] = $protocol . '://' . $host . "/index.php?route=extension%2Fopenidconnect%2Fmodule%2Fopenidconnect.callback";

        $data['header']       = $this->load->controller('common/header');
        $data['column_left']  = $this->load->controller('common/column_left');
        $data['footer']       = $this->load->controller('common/footer');

        $data['action'] = $this->url->link('extension/openidconnect/module/openidconnect', 'user_token=' . $this->session->data['user_token'], true);

        $this->response->setOutput($this->load->view('extension/openidconnect/module/openidconnect', $data));
    }
}
