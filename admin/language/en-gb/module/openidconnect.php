<?php
$_['heading_title']                         = 'OpenID Connect';

$_['text_settings']                         = 'OpenID Connect settings';

$_['text_client_authentication']            = 'Client authentication';
$_['text_client_id']                        = 'Client ID';
$_['text_client_secret']                    = 'Client secret';

$_['text_idp_endpoints']                    = 'Identity provider endpoints';
$_['text_authorization_url']                = 'Authorization endpoint URL';
$_['text_token_url']                        = 'Token endpoint URL';
$_['text_userinfo_url']                     = 'Userinfo endpoint URL';

$_['text_openid_details']                   = 'OpenID details';
$_['text_scope']                            = 'Scope';
$_['text_require_prompt']                   = 'Always require login prompt from IdP';

$_['text_generated_data']                   = 'Generated data';
$_['text_redirect_url']                     = 'Redirect URL';

$_['button_save']                           = 'Save';
