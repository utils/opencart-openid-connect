<?php

namespace Opencart\Catalog\Controller\Extension\Openidconnect\Module;
//for 4.0.0.0

class Openidconnect extends \Opencart\System\Engine\Controller
{
    private $redirect_url = "";

    public function __construct(\Opencart\System\Engine\Registry $registry)
    {
        parent::__construct($registry);

        $this->config->addPath('extension/openidconnect', DIR_EXTENSION . 'openidconnect/system/config');
        $this->redirect_url = $this->config->get('config_url') . "index.php?route=extension%2Fopenidconnect%2Fmodule%2Fopenidconnect.callback";
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function login(): void
    {
        $config = $this->model_setting_setting->getSetting('openidconnect');

        if (array_key_exists('openidconnect_authorization_url', $config)) {
            $this->response->redirect(
                $config['openidconnect_authorization_url'] .
                    "?client_id=" . $config["openidconnect_client_id"] .
                    "&response_type=code" .
                    "&redirect_uri=" . urlencode($this->redirect_url) .
                    "&state=" . $this->generateRandomString() .
                    "&scope=" . $config['openidconnect_scope'] .
                    ($config["openidconnect_require_prompt"] ? "&prompt=consent" : "")
            );
        } else {
            $this->response->redirect('/index.php?route=extension/openidconnect/module/openidconnect.error&error=Authorization URL not set.');
        }
    }

    public function error(): void
    {
        $this->load->language('extension/openidconnect/module/openidconnect');

        $this->load->config('extension/openidconnect/openidconnect');
        $this->document->setTitle($this->language->get('heading_title'));

        $data = array();

        $data['header']       = $this->load->controller('common/header');
        $data['column_left']  = $this->load->controller('common/column_left');
        $data['footer']       = $this->load->controller('common/footer');

        $data['error_message'] = $this->request->get['error'];

        $this->response->setOutput($this->load->view('extension/openidconnect/module/error', $data));
    }

    public function callback(): void
    {
        $config = $this->model_setting_setting->getSetting('openidconnect');

        // Make request to token endpoint
        $token_request = curl_init();
        $token_endpoint = $config['openidconnect_token_url'];
        curl_setopt($token_request, CURLOPT_URL, $token_endpoint);
        curl_setopt($token_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($token_request, CURLOPT_POST, true);

        $post_params = array(
            'grant_type' => 'authorization_code',
            'code' => $this->request->get['code'],
            'redirect_uri' => $this->redirect_url
        );

        curl_setopt($token_request, CURLOPT_POSTFIELDS, http_build_query($post_params));

        curl_setopt($token_request, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($config['openidconnect_client_id'] . ":" . $config['openidconnect_client_secret'])]);

        $token_result = curl_exec($token_request);
        $token_object = json_decode($token_result);

        curl_close($token_request);

        if (is_null($token_object) || !property_exists($token_object, "access_token")) {
            $this->response->redirect('/index.php?route=extension/openidconnect/module/openidconnect.error&error=Authentication failed at token request.');
            return;
        }

        // Make request to userinfo endpoint
        $userinfo_request = curl_init();
        $userinfo_endpoint = $config['openidconnect_userinfo_url'];
        curl_setopt($userinfo_request, CURLOPT_URL, $userinfo_endpoint);
        curl_setopt($userinfo_request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($userinfo_request, CURLOPT_HTTPHEADER, ["Authorization: Bearer " . $token_object->access_token]);

        $userinfo_result = curl_exec($userinfo_request);
        $userinfo_object = json_decode($userinfo_result);

        curl_close($userinfo_request);

        if (!property_exists($userinfo_object, "email")) {
            $this->response->redirect('/index.php?route=extension/openidconnect/module/openidconnect.error&error=Authentication failed at userinfo request. Email is required.');
        }

        $this->load->model('account/customer');

        $customer_info = $this->model_account_customer->getCustomerByEmail($userinfo_object->email);

        if (!$customer_info) {
            $this->load->model('account/customer_group');

            $customer_group_id = (int)$this->config->get('config_customer_group_id');
            $customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

            $customer_id = $this->model_account_customer->addCustomer([
                'email' => $userinfo_object->email,
                'firstname' => $userinfo_object->given_name ?: 'User',
                'lastname' => $userinfo_object->family_name ?: 'User',
                'telephone' => '',
                'password' => '',
                'customer_group_id' => $customer_group_id
            ]);

            if (!$customer_group_info['approval']) {
                $this->customer->login($userinfo_object->email, '', true);

                // Add customer details into session
                $this->session->data['customer'] = [
                    'customer_id'       => $customer_id,
                    'customer_group_id' => $customer_group_id,
                    'firstname'         => $userinfo_object->given_name,
                    'lastname'          => $userinfo_object->family_name,
                    'email'             => $userinfo_object->email,
                    'telephone'         => '',
                    'custom_field'      => '',
                ];
            }
        } else {
            $this->customer->login($userinfo_object->email, "", true);

            $this->session->data['customer'] = [
                'customer_id'       => $customer_info['customer_id'],
                'customer_group_id' => $customer_info['customer_group_id'],
                'firstname'         => $customer_info['firstname'],
                'lastname'          => $customer_info['lastname'],
                'email'             => $customer_info['email'],
                'telephone'         => $customer_info['telephone'],
                'custom_field'      => $customer_info['custom_field']
            ];

            unset($this->session->data['order_id']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);

            // Wishlist
            if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
                $this->load->model('account/wishlist');

                foreach ($this->session->data['wishlist'] as $key => $product_id) {
                    $this->model_account_wishlist->addWishlist($product_id);

                    unset($this->session->data['wishlist'][$key]);
                }
            }
        }

        // Log the IP info
        $this->model_account_customer->addLogin($this->customer->getId(), $this->request->server['REMOTE_ADDR']);

        // Create customer token
        $this->session->data['customer_token'] = oc_token(26);

        // Clear any previous login attempts for unregistered accounts.
        $this->model_account_customer->deleteLoginAttempts($userinfo_object->email);

        $this->response->redirect('/?customer_token=' . $this->session->data['customer_token']);
    }
}
